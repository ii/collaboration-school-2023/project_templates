import matplotlib.pyplot as plt

from tools.text_tools import make_all_capital
from tools.text_tools import decorate
from tools.general import create_directory


if __name__ == "__main__":
    user_input = input("Please enter a word or phrase: ")
    output = make_all_capital(user_input)
    output = decorate(output)

    fig, ax = plt.subplots(figsize=(4,4))

    text_kwargs = dict(ha='center', va='center', fontsize=28, color='C1')
    ax.text(.5,.5,output, **text_kwargs)
    ax.axis('off')

    create_directory('out')
    fig.savefig('out/fabules.jpg')
