# Project_templates



## Getting started

This programme makes your input text fabulace and saves it as an image.

## Usage

Run the programme using a python interpreter:

```python main.py```

Enter an input word or phrase. A text version of the modified input will be displayed on the terminal and a figure containing the text will be saved in an 'out' directory.