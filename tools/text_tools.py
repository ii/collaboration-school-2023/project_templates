def make_all_capital(x):
    """Make all letters in the target string (x) capital.

    Parameters
    ----------
    x : str
        target string.
    
    Returns
    -------
    string
        A string made up of capital letters

    Examples
    --------
    >>> make_all_cap('ali')
    ALI
    >>> make_all_cap('david')
    DAVID
    >>> make_all_cap('hello world!')
    HELLO WORLD!
    """
    return x.upper()

def decorate(x, decor = "***"):
    """Decorate the input with a decoration string 

    Add the decoration string to the beginning and the end of the target string

    Parameters
    ----------
    x : str
       target string
    decor : str
        String that will get added to the beginning and the end of the target string.

    Returns
    -------
    str
        String that begins and ends with the decor and contains x


    Examples
    --------
    >>> decorate('ali')
    *** ali ***
    >>> decorate('ali', ^^)
    ^^ ali ^^
    >>> decorate('hello world!', :::)
    ::: hello world! :::
    """
    return f'{decor} {x} {decor}'